# start by pulling the python image
FROM python:3.8-alpine

# RUN apk add mariadb-dev
# copy the requirements file into the image
COPY ./requirements.txt /app/requirements.txt

# switch working directory
WORKDIR /app

# install the dependencies and packages in the requirements file
# RUN pip install -r requirements.txt
# RUN apk add --no-cache --virtual .build-deps gcc libffi-dev openssl-dev musl-dev mariadb-dev mariadb-connector-c-dev\
#     && pip install --no-cache-dir -r requirements.txt \
#     && apk del .build-deps gcc libffi-dev musl-dev openssl-dev mariadb-dev

RUN apk add --no-cache gcc libffi-dev openssl-dev musl-dev mariadb-dev mariadb-connector-c-dev\
    && pip install --no-cache-dir -r requirements.txt 

# copy every content from the local file to the image
COPY . /app

# configure the container to run in an executed manner
ENTRYPOINT [ "python" ]
CMD ["index.py" ]